package ru.t1.dkozoriz.tm.api.repository.model;

import ru.t1.dkozoriz.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {
}
