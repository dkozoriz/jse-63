package ru.t1.dkozoriz.tm.repository.model.business;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.dkozoriz.tm.model.business.Task;

import java.util.List;

@Repository
@Scope("prototype")
public interface TaskRepository extends BusinessRepository<Task> {

    @NotNull
    List<Task> findAllByProjectIdAndUserId(@Nullable String userId, @NotNull String projectId);

}