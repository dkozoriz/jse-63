package ru.t1.dkozoriz.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.model.business.BusinessModel;

import java.util.List;

public interface IBusinessRepository<T extends BusinessModel> extends IUserOwnedRepository<T> {

    @NotNull List<T> findAllOrderByName(@NotNull String userId);

    @NotNull List<T> findAllOrderByStatus(@NotNull String userId);

    @NotNull List<T> findAllOrderByCreated(@NotNull String userId);

}