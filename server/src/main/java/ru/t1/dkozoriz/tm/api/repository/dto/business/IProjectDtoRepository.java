package ru.t1.dkozoriz.tm.api.repository.dto.business;


import ru.t1.dkozoriz.tm.dto.model.business.ProjectDto;

public interface IProjectDtoRepository extends IBusinessDtoRepository<ProjectDto> {

}