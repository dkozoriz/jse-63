<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../include/_header.jsp"/>
<h1>Project edit</h1>

<form action="/project/edit/?id=${project.id}" method="post">
    <input type="hidden" name="id" value="${project.id}"/>

    <p>
    <div>Name:</div>
    <div><input type="text" name="name" value="${project.name}"/></div>

    <p>
    <div>Description:</div>
    <div><input type="text" name="description" value="${project.description}"/></div>

    <button type="submit">Save project</button>
</form>

<jsp:include page="../include/_footer.jsp"/>

