<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>

<h1>TASK LIST</h1>
<table>
    <tr>
        <th scope="col">ID</th>
        <th scope="col">PROJECT ID</th>
        <th scope="col">NAME</th>
        <th scope="col">DESCRIPTION</th>
        <th scope="col">STATUS</th>
        <th scope="col">CREATED</th>
        <th scope="col">EDIT</th>
        <th scope="col">DELETE</th>
    </tr>
    <c:forEach var="task" items="${tasks}">
        <tr>
            <td><c:out value="${task.id}"/></td>
            <td><c:out value="${task.projectId}"/></td>
            <td><c:out value="${task.name}"/></td>
            <td><c:out value="${task.description}"/></td>
            <td><c:out value="${task.status.displayName}"/></td>
            <td><fmt:formatDate value="${task.created}"/></td>
            <td><a href="/task/edit/?id=${task.id}"/>EDIT</td>
            <td><a href="/task/delete/?id=${task.id}"/>DELETE</td>
        </tr>
    </c:forEach>
</table>
<br/>
<form action="/task/create" style="margin-top: 20px">
    <button>CREATE TASK</button>
</form>

<jsp:include page="../include/_footer.jsp"/>