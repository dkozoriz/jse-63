package ru.t1.dkozoriz.tm.servlet.task;

import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.model.Task;
import ru.t1.dkozoriz.tm.repository.TaskRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/task/edit/*")
public class TaskEditServlet extends HttpServlet {

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        final String id = req.getParameter("id");
        final Task task = TaskRepository.getInstance().findById(id);
        req.setAttribute("task", task);
        req.getRequestDispatcher("/WEB-INF/views/task-edit.jsp").forward(req, resp);
    }


    @Override
    protected void doPost(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        final String id = req.getParameter("id");
        final String name = req.getParameter("name");
        final String description = req.getParameter("description");
        final Task task = TaskRepository.getInstance().findById(id);
        task.setName(name);
        task.setDescription(description);
        TaskRepository.getInstance().save(task);
        resp.sendRedirect("/tasks");
    }

}