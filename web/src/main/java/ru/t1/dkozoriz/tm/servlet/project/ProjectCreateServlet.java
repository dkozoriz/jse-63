package ru.t1.dkozoriz.tm.servlet.project;

import ru.t1.dkozoriz.tm.repository.ProjectRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/project/create/*")
public class ProjectCreateServlet extends HttpServlet {

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        ProjectRepository.getInstance().create();
        response.sendRedirect("/projects");
    }

}