package ru.t1.dkozoriz.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.dkozoriz.tm.enumerated.Status;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;


@Getter
@Setter
@NoArgsConstructor
public class Task implements Serializable {

    private static final long serialVersionUID = 1;

    private String id = UUID.randomUUID().toString();

    private String name = "";

    private String description = "";

    private Status status = Status.NOT_STARTED;

    private Date created = new Date();

    private String projectId = "";

    public Task(final String name) {
        this.name = name;
    }

    public Task(final String name, final String description) {
        this.name = name;
        this.description = description;
    }

}