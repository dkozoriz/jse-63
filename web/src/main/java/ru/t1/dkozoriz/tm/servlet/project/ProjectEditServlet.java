package ru.t1.dkozoriz.tm.servlet.project;

import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.model.Project;
import ru.t1.dkozoriz.tm.repository.ProjectRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/project/edit/*")
public class ProjectEditServlet extends HttpServlet {

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        final String id = req.getParameter("id");
        final Project project = ProjectRepository.getInstance().findById(id);
        req.setAttribute("project", project);
        req.getRequestDispatcher("/WEB-INF/views/project-edit.jsp").forward(req, resp);
    }


    @Override
    protected void doPost(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        final String id = req.getParameter("id");
        final String name = req.getParameter("name");
        final String description = req.getParameter("description");
        final Project project = ProjectRepository.getInstance().findById(id);
        project.setName(name);
        project.setDescription(description);
        ProjectRepository.getInstance().save(project);
        resp.sendRedirect("/projects");
    }

}