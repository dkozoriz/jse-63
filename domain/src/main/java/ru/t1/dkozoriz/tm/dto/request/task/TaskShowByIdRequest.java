package ru.t1.dkozoriz.tm.dto.request.task;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TaskShowByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    public TaskShowByIdRequest(
            @Nullable final String token,
            @Nullable String id
    ) {
        super(token);
        this.id = id;
    }

}