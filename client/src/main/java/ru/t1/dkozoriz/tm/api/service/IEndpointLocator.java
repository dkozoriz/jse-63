package ru.t1.dkozoriz.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.api.endpoint.*;

public interface IEndpointLocator {

    @NotNull
    IAuthEndpoint getAuthEndpoint();

    @NotNull
    IProjectEndpoint getProjectEndpoint();

    @NotNull
    ITaskEndpoint getTaskEndpoint();

    @NotNull
    ISystemEndpoint getSystemEndpoint();

    @NotNull
    IUserEndpoint getUserEndpoint();

}