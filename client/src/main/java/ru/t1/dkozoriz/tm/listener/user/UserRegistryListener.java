package ru.t1.dkozoriz.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.dto.model.UserDto;
import ru.t1.dkozoriz.tm.dto.request.user.UserRegistryRequest;
import ru.t1.dkozoriz.tm.event.ConsoleEvent;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

@Component
public final class UserRegistryListener extends AbstractUserListener {

    public UserRegistryListener() {
        super("user-registry", "registry user.");
    }

    @Override
    @EventListener(condition = "@userRegistryListener.getName() == #consoleEvent.name")
    public void handler(@NotNull ConsoleEvent consoleEvent) {
        System.out.println("[USER REGISTRY]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @Nullable final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        @Nullable final String email = TerminalUtil.nextLine();
        @NotNull final UserDto user = userEndpoint.userRegistry(new UserRegistryRequest(login, password, email)).getUser();
        showUser(user);
    }

}